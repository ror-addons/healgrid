local textures = {
    { id="tint_square",	texture="tint_square",		label=L"Tint Square",		dims={w=32,h=32},			tags={"statusbar","background",},	},

    { id="HalA",			texture="HalA",				label=L"Hal A",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalB",			texture="HalB",				label=L"Hal B",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalC",			texture="HalC",				label=L"Hal C",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalD",			texture="HalD",				label=L"Hal D",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalE",			texture="HalE",				label=L"Hal E",					dims={w=128,h=128},			tags={"statusbar",},	},
    { id="HalF",			texture="HalF",				label=L"Hal F",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalG",			texture="HalG",				label=L"Hal G",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalH",			texture="HalH",				label=L"Hal H",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalI",			texture="HalI",				label=L"Hal I",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalJ",			texture="HalJ",				label=L"Hal J",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalK",			texture="HalK",				label=L"Hal K",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalL",			texture="HalL",				label=L"Hal L",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalM",			texture="HalM",				label=L"Hal M",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalN",			texture="HalN",				label=L"Hal N",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalO",			texture="HalO",				label=L"Hal O",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalP",			texture="HalP",				label=L"Hal P",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalQ",			texture="HalQ",				label=L"Hal Q",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalR",			texture="HalR",				label=L"Hal R",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalS",			texture="HalS",				label=L"Hal S",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalT",			texture="HalT",				label=L"Hal T",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalU",			texture="HalU",				label=L"Hal U",					dims={w=256,h=32},			tags={"statusbar",},	},
    { id="HalV",			texture="HalV",				label=L"Hal V",					dims={w=256,h=32},			tags={"statusbar",},	},

	{ id="Aluminium",		texture="Aluminium",		label=L"Aluminium",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Armory",			texture="Armory",			label=L"Armory",				dims={w=16,h=16},			tags={"statusbar",},	},
	{ id="BantoBar",		texture="BantoBar",			label=L"Banto Bar",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Bars",			texture="Bars",				label=L"Bars",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Bumps",			texture="Bumps",			label=L"Bumps",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Button",			texture="Button",			label=L"Button",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Charcoal",		texture="Charcoal",			label=L"Charcoal",				dims={w=128,h=128},			tags={"statusbar",},	},
	{ id="Cilo",			texture="Cilo",				label=L"Cilo",					dims={w=128,h=128},			tags={"statusbar",},	},
	{ id="Cloud",			texture="Cloud",			label=L"Cloud",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Combo",			texture="Combo",			label=L"Combo",					dims={w=16,h=16},			tags={"statusbar",},	},
	{ id="Comet",			texture="Comet",			label=L"Comet",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Dabs",			texture="Dabs",				label=L"Dabs",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="DarkBottom",		texture="DarkBottom",		label=L"Dark Bottom",			dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Diagonal",		texture="Diagonal",			label=L"Diagonal",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Empty",			texture="Empty",			label=L"Empty",					dims={w=16,h=16},			tags={"statusbar",},	},
	{ id="Falumn",			texture="Falumn",			label=L"Falumn",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Fifths",			texture="Fifths",			label=L"Fifths",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Flat",			texture="Flat",				label=L"Flat",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Fourths",			texture="Fourths",			label=L"Fourths",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Frost",			texture="Frost",			label=L"Frost",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Glamour",			texture="Glamour",			label=L"Glamour",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glamour2",		texture="Glamour2",			label=L"Glamour 2",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glamour3",		texture="Glamour3",			label=L"Glamour 3",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glamour4",		texture="Glamour4",			label=L"Glamour 4",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glamour5",		texture="Glamour5",			label=L"Glamour 5",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glamour6",		texture="Glamour6",			label=L"Glamour 6",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glamour7",		texture="Glamour7",			label=L"Glamour 7",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Glass",			texture="Glass",			label=L"Glass",					dims={w=64,h=32},			tags={"statusbar",},	},
	{ id="Glaze",			texture="Glaze",			label=L"Glaze",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Glaze2",			texture="Glaze2",			label=L"Glaze 2",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Gloss",			texture="Gloss",			label=L"Gloss",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Graphite",		texture="Graphite",			label=L"Graphite",				dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Grid",			texture="Grid",				label=L"Grid",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Hatched",			texture="Hatched",			label=L"Hatched",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Healbot",			texture="Healbot",			label=L"Healbot",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="LiteStep",		texture="LiteStep",			label=L"Lite Step",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="LiteStepLite",	texture="LiteStepLite",		label=L"Lite Step Lite",		dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Lyfe",			texture="Lyfe",				label=L"Lyfe",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Melli",			texture="Melli",			label=L"Melli",					dims={w=64,h=64},			tags={"statusbar",},	},
	{ id="MelliDark",		texture="MelliDark",		label=L"Melli Dark",			dims={w=64,h=64},			tags={"statusbar",},	},
	{ id="MelliDarkRough",	texture="MelliDarkRough",	label=L"Melli Dark Rough",		dims={w=64,h=64},			tags={"statusbar",},	},
	{ id="Minimalist",		texture="Minimalist",		label=L"Minimalist",			dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Otravi",			texture="Otravi",			label=L"Otravi",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Outline",			texture="Outline",			label=L"Outline",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Perl",			texture="Perl",				label=L"Perl",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Perl2",			texture="Perl2",			label=L"Perl 2",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Pill",			texture="Pill",				label=L"Pill",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Rain",			texture="Rain",				label=L"Rain",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Rocks",			texture="Rocks",			label=L"Rocks",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Round",			texture="Round",			label=L"Round",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Ruben",			texture="Ruben",			label=L"Ruben",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Runes",			texture="Runes",			label=L"Runes",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Skewed",			texture="Skewed",			label=L"Skewed",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Smooth",			texture="Smooth",			label=L"Smo0th",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Smoothv2",		texture="Smoothv2",			label=L"Smooth v2",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Smudge",			texture="Smudge",			label=L"Smudge",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Steel",			texture="Steel",			label=L"Steel",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Striped",			texture="Striped",			label=L"Striped",				dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Tube",			texture="Tube",				label=L"Tube",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Water",			texture="Water",			label=L"Water",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Wglass",			texture="Wglass",			label=L"Water Glass",			dims={w=256,h=64},			tags={"statusbar",},	},
	{ id="Wisps",			texture="Wisps",			label=L"Wisps",					dims={w=256,h=32},			tags={"statusbar",},	},
	{ id="Xeon",			texture="Xeon",				label=L"Xeon",					dims={w=256,h=32},			tags={"statusbar",},	},

    { id="HalBackground",	texture="HalBackground",	label=L"Hal Background",		dims={w=256,h=256},			tags={"background",},	},
    { id="HalBackgroundA",	texture="HalBackgroundA",	label=L"Hal Background B",		dims={w=256,h=256},			tags={"background",},	},

    { id="Moo",				texture="Moo",				label=L"Moo",					dims={w=256,h=256},			tags={"background",},	},

};

for k,t in ipairs(textures) do
	HealGridSkin.TextureRegister( t.id, t.texture, t.label, t.dims, t.tags );
end
