HealGridSkin_SharpXL = {

	skin = {
		id = "SHARP_XL",
		name = L"Sharp - XL",
		author = "rmet0815",
		sortCriteria = "SHARP_40",
	},

	hud = {
		buffs = {
			size = 36,
			border = 2,
			textPadding = 1,
			font = "font_clear_small_bold",
		},
		unitFrame = {
			width = 80,
			height = 80,
			borderWidth = 2,
			borderHeight = 2,
		},
		avatarFrame = {
			spellTrack1 = {
				font = "font_clear_large",
			},
			spellTrack2 = {
				font = "font_clear_large",
			},
			labelTopLeft = {
				font = "font_clear_large",
			},
			labelTop = {
				font = "font_clear_large",
			},
			labelTopRight = {
				font = "font_clear_large",
			},
			labelLeft = {
				font = "font_clear_large",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_large",
			},
			labelBottomLeft = {
				font = "font_clear_large",
			},
			labelBottom = {
				font = "font_clear_large",
			},
			labelBottomRight = {
				font = "font_clear_large",
			},
		},
		petFrame = {
			spellTrack1 = {
				font = "font_clear_large",
			},
			spellTrack2 = {
				font = "font_clear_large",
			},
			labelTopLeft = {
				font = "font_clear_large",
			},
			labelTop = {
				font = "font_clear_large",
			},
			labelTopRight = {
				font = "font_clear_large",
			},
			labelLeft = {
				font = "font_clear_large",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_large",
			},
			labelBottomLeft = {
				font = "font_clear_large",
			},
			labelBottom = {
				font = "font_clear_large",
			},
			labelBottomRight = {
				font = "font_clear_large",
			},
		},
		friendlyTargetFrame = {
			spellTrack1 = {
				font = "font_clear_large",
			},
			spellTrack2 = {
				font = "font_clear_large",
			},
			labelTopLeft = {
				font = "font_clear_large",
			},
			labelTop = {
				font = "font_clear_large",
			},
			labelTopRight = {
				font = "font_clear_large",
			},
			labelLeft = {
				font = "font_clear_large",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_large",
			},
			labelBottomLeft = {
				font = "font_clear_large",
			},
			labelBottom = {
				font = "font_clear_large",
			},
			labelBottomRight = {
				font = "font_clear_large",
			},
		},
		hostileTargetFrame = {
			spellTrack1 = {
				font = "font_clear_large",
			},
			spellTrack2 = {
				font = "font_clear_large",
			},
			labelTopLeft = {
				font = "font_clear_large",
			},
			labelTop = {
				font = "font_clear_large",
			},
			labelTopRight = {
				font = "font_clear_large",
			},
			labelLeft = {
				font = "font_clear_large",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_large",
			},
			labelBottomLeft = {
				font = "font_clear_large",
			},
			labelBottom = {
				font = "font_clear_large",
			},
			labelBottomRight = {
				font = "font_clear_large",
			},
		},
		petTargetFrame = {
			spellTrack1 = {
				font = "font_clear_large",
			},
			spellTrack2 = {
				font = "font_clear_large",
			},
			labelTopLeft = {
				font = "font_clear_large",
			},
			labelTop = {
				font = "font_clear_large",
			},
			labelTopRight = {
				font = "font_clear_large",
			},
			labelLeft = {
				font = "font_clear_large",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_large",
			},
			labelBottomLeft = {
				font = "font_clear_large",
			},
			labelBottom = {
				font = "font_clear_large",
			},
			labelBottomRight = {
				font = "font_clear_large",
			},
		},
		actionPointFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_medium_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
		},
		careerFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_medium_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
		},
		moraleFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_alert_outline_half_large",
			},
			labelCenter = {
				font = "font_alert_outline_half_large",
			},
			labelRight = {
				font = "font_alert_outline_half_large",
			},
		},
		castFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_medium_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
		},
		target = {
			font = "font_clear_large_bold",
		},
	},

	grid = {
		unitFrame = {
			width = 80,
			height = 80,
			borderWidth = 2,
			borderHeight = 2,
			spellTrack1 = {
				font = "font_clear_large",
			},
			spellTrack2 = {
				font = "font_clear_large",
			},
			labelTopLeft = {
				font = "font_clear_large",
			},
			labelTop = {
				font = "font_clear_large",
			},
			labelTopRight = {
				font = "font_clear_large",
			},
			labelLeft = {
				font = "font_clear_large",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_large",
			},
			labelBottomLeft = {
				label = L"[name]",
				font = "font_clear_large",
			},
			labelBottom= {
				label = L"",
				font = "font_clear_large",
			},
			labelBottomRight = {
				label = L"",
				font = "font_clear_large",
			},
		},
	},

};

HealGridSkin.RegisterSkin( HealGridSkin_SharpXL, HealGridSkin.BASE_SKIN_ID );
