HealGridSkin_SharpSmall = {

	skin = {
		id = "SHARP_SMALL",
		name = L"Sharp - Small",
		author = "rmet0815",
		sortCriteria = "SHARP_10",
	},

	hud = {
		buffs = {
			size = 24,
			border = 1,
			textPadding = 1,
			font = "font_clear_small_bold",
		},
		unitFrame = {
			width = 50,
			height = 50,
			borderWidth = 2,
			borderHeight = 2,
		},
		avatarFrame = {
			spellTrack1 = {
				font = "font_clear_tiny",
			},
			spellTrack2 = {
				font = "font_clear_tiny",
			},
			labelTopLeft = {
				font = "font_clear_tiny",
			},
			labelTop = {
				font = "font_clear_tiny",
			},
			labelTopRight = {
				font = "font_clear_tiny",
			},
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_small_bold",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
			labelBottomLeft = {
				font = "font_clear_tiny",
			},
			labelBottom = {
				font = "font_clear_tiny",
			},
			labelBottomRight = {
				font = "font_clear_tiny",
			},
		},
		petFrame = {
			spellTrack1 = {
				font = "font_clear_tiny",
			},
			spellTrack2 = {
				font = "font_clear_tiny",
			},
			labelTopLeft = {
				font = "font_clear_tiny",
			},
			labelTop = {
				font = "font_clear_tiny",
			},
			labelTopRight = {
				font = "font_clear_tiny",
			},
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_small_bold",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
			labelBottomLeft = {
				font = "font_clear_tiny",
			},
			labelBottom = {
				font = "font_clear_tiny",
			},
			labelBottomRight = {
				font = "font_clear_tiny",
			},
		},
		friendlyTargetFrame = {
			spellTrack1 = {
				font = "font_clear_tiny",
			},
			spellTrack2 = {
				font = "font_clear_tiny",
			},
			labelTopLeft = {
				font = "font_clear_tiny",
			},
			labelTop = {
				font = "font_clear_tiny",
			},
			labelTopRight = {
				font = "font_clear_tiny",
			},
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_small_bold",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
			labelBottomLeft = {
				font = "font_clear_tiny",
			},
			labelBottom = {
				font = "font_clear_tiny",
			},
			labelBottomRight = {
				font = "font_clear_tiny",
			},
		},
		hostileTargetFrame = {
			spellTrack1 = {
				font = "font_clear_tiny",
			},
			spellTrack2 = {
				font = "font_clear_tiny",
			},
			labelTopLeft = {
				font = "font_clear_tiny",
			},
			labelTop = {
				font = "font_clear_tiny",
			},
			labelTopRight = {
				font = "font_clear_tiny",
			},
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_small_bold",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
			labelBottomLeft = {
				font = "font_clear_tiny",
			},
			labelBottom = {
				font = "font_clear_tiny",
			},
			labelBottomRight = {
				font = "font_clear_tiny",
			},
		},
		petTargetFrame = {
			spellTrack1 = {
				font = "font_clear_tiny",
			},
			spellTrack2 = {
				font = "font_clear_tiny",
			},
			labelTopLeft = {
				font = "font_clear_tiny",
			},
			labelTop = {
				font = "font_clear_tiny",
			},
			labelTopRight = {
				font = "font_clear_tiny",
			},
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_small_bold",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
			labelBottomLeft = {
				font = "font_clear_tiny",
			},
			labelBottom = {
				font = "font_clear_tiny",
			},
			labelBottomRight = {
				font = "font_clear_tiny",
			},
		},
		actionPointFrame = {
			height = 16,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_tiny",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
		},
		careerFrame = {
			height = 16,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_tiny",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
		},
		moraleFrame = {
			height = 16,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_alert_outline_half_small",
			},
			labelCenter = {
				font = "font_alert_outline_half_small",
			},
			labelRight = {
				font = "font_alert_outline_half_small",
			},
		},
		castFrame = {
			height = 16,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_tiny",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
		},
		target = {
			font = "font_clear_small_bold",
		},
	},

	grid = {
		unitFrame = {
			width = 50,
			height = 50,
			borderWidth = 2,
			borderHeight = 2,
			spellTrack1 = {
				font = "font_clear_tiny",
			},
			spellTrack2 = {
				font = "font_clear_tiny",
			},
			labelTopLeft = {
				font = "font_clear_tiny",
			},
			labelTop = {
				font = "font_clear_tiny",
			},
			labelTopRight = {
				font = "font_clear_tiny",
			},
			labelLeft = {
				font = "font_clear_tiny",
			},
			labelCenter = {
				font = "font_clear_small_bold",
			},
			labelRight = {
				font = "font_clear_tiny",
			},
			labelBottomLeft = {
				font = "font_clear_tiny",
			},
			labelBottom = {
				font = "font_clear_tiny",
			},
			labelBottomRight = {
				font = "font_clear_tiny",
			},
		},
	},

};

HealGridSkin.RegisterSkin( HealGridSkin_SharpSmall, HealGridSkin.BASE_SKIN_ID );
