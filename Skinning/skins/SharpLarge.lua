HealGridSkin_SharpLarge = {

	skin = {
		id = "SHARP_LARGE",
		name = L"Sharp - Large",
		author = "rmet0815",
		sortCriteria = "SHARP_30",
	},

	hud = {
		buffs = {
			size = 36,
			border = 2,
			textPadding = 1,
			font = "font_clear_small_bold",
		},
		unitFrame = {
			width = 70,
			height = 70,
			borderWidth = 2,
			borderHeight = 2,
		},
		avatarFrame = {
			spellTrack1 = {
				font = "font_clear_medium",
			},
			spellTrack2 = {
				font = "font_clear_medium",
			},
			labelTopLeft = {
				font = "font_clear_medium",
			},
			labelTop = {
				font = "font_clear_medium",
			},
			labelTopRight = {
				font = "font_clear_medium",
			},
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
			labelBottomLeft = {
				font = "font_clear_medium",
			},
			labelBottom = {
				font = "font_clear_medium",
			},
			labelBottomRight = {
				font = "font_clear_medium",
			},
		},
		petFrame = {
			spellTrack1 = {
				font = "font_clear_medium",
			},
			spellTrack2 = {
				font = "font_clear_medium",
			},
			labelTopLeft = {
				font = "font_clear_medium",
			},
			labelTop = {
				font = "font_clear_medium",
			},
			labelTopRight = {
				font = "font_clear_medium",
			},
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
			labelBottomLeft = {
				font = "font_clear_medium",
			},
			labelBottom = {
				font = "font_clear_medium",
			},
			labelBottomRight = {
				font = "font_clear_medium",
			},
		},
		friendlyTargetFrame = {
			spellTrack1 = {
				font = "font_clear_medium",
			},
			spellTrack2 = {
				font = "font_clear_medium",
			},
			labelTopLeft = {
				font = "font_clear_medium",
			},
			labelTop = {
				font = "font_clear_medium",
			},
			labelTopRight = {
				font = "font_clear_medium",
			},
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
			labelBottomLeft = {
				font = "font_clear_medium",
			},
			labelBottom = {
				font = "font_clear_medium",
			},
			labelBottomRight = {
				font = "font_clear_medium",
			},
		},
		hostileTargetFrame = {
			spellTrack1 = {
				font = "font_clear_medium",
			},
			spellTrack2 = {
				font = "font_clear_medium",
			},
			labelTopLeft = {
				font = "font_clear_medium",
			},
			labelTop = {
				font = "font_clear_medium",
			},
			labelTopRight = {
				font = "font_clear_medium",
			},
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
			labelBottomLeft = {
				font = "font_clear_medium",
			},
			labelBottom = {
				font = "font_clear_medium",
			},
			labelBottomRight = {
				font = "font_clear_medium",
			},
		},
		petTargetFrame = {
			spellTrack1 = {
				font = "font_clear_medium",
			},
			spellTrack2 = {
				font = "font_clear_medium",
			},
			labelTopLeft = {
				font = "font_clear_medium",
			},
			labelTop = {
				font = "font_clear_medium",
			},
			labelTopRight = {
				font = "font_clear_medium",
			},
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
			labelBottomLeft = {
				font = "font_clear_medium",
			},
			labelBottom = {
				font = "font_clear_medium",
			},
			labelBottomRight = {
				font = "font_clear_medium",
			},
		},
		actionPointFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_medium_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
		},
		careerFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_medium_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
		},
		moraleFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_alert_outline_half_large",
			},
			labelCenter = {
				font = "font_alert_outline_half_large",
			},
			labelRight = {
				font = "font_alert_outline_half_large",
			},
		},
		castFrame = {
			height = 20,
			borderWidth = 1,
			borderHeight = 1,
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_medium_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
		},
		target = {
			font = "font_clear_large_bold",
		},
	},

	grid = {
		unitFrame = {
			width = 70,
			height = 70,
			borderWidth = 2,
			borderHeight = 2,
			spellTrack1 = {
				font = "font_clear_medium",
			},
			spellTrack2 = {
				font = "font_clear_medium",
			},
			labelTopLeft = {
				font = "font_clear_medium",
			},
			labelTop = {
				font = "font_clear_medium",
			},
			labelTopRight = {
				font = "font_clear_medium",
			},
			labelLeft = {
				font = "font_clear_medium",
			},
			labelCenter = {
				font = "font_clear_large_bold",
			},
			labelRight = {
				font = "font_clear_medium",
			},
			labelBottomLeft = {
				font = "font_clear_medium",
			},
			labelBottom = {
				font = "font_clear_medium",
			},
			labelBottomRight = {
				font = "font_clear_medium",
			},
		},
	},

};

HealGridSkin.RegisterSkin( HealGridSkin_SharpLarge, HealGridSkin.BASE_SKIN_ID );
