HealGridSkin_SharpNormal = {

	skin = {
		id = "SHARP_MEDIUM",
		name = L"Sharp - Medium",
		author = "rmet0815",
		sortCriteria = "SHARP_20",
	},

};

HealGridSkin.RegisterSkin( HealGridSkin_SharpNormal, HealGridSkin.BASE_SKIN_ID );
