HealGridSkin_LucidMedium = {

	skin = {
		id = "LUCID_MEDIUM",
		name = L"Lucid - Medium",
		author = "rmet0815",
		sortCriteria = "LUCID_20",
	},

	grid = {
		orientation = 'VERTICAL',
		growth = 'DOWN',
		unitFrame = {
			width = 200,
			height = 60,
			borderWidth = 2,
			borderHeight = 2,
			background = {
				color = "HEALTHCOLOR",
				texture = "WARTintSquare",
				style = "TILE",
			},
			careerLineIcon = {
				scale = 80,
				alpha = 100,
				position = 'LEFT',
				paddingX = "2",
				paddingY = "0",
				isVisible = true,
			},
			healthPointsBar = {
				anchor1 = { windowName="SELF", point="TOPLEFT", relativeTo="PARENT", relativePoint="TOPLEFT", offsetX="25%", offsetY="30%", },
				anchor2 = { windowName="SELF", point="TOPRIGHT", relativeTo="PARENT", relativePoint="BOTTOMRIGHT", offsetX="0", offsetY="65%", },
				growth = "LEFT_RIGHT",
				foreground = {
					isVisible = true,
					color = "GREEN",
					texture = "Otravi",
					style = "FILL",
				},
				background = {
					isVisible = true,
					color = "SEA_GREEN",
					texture = "WARTintSquare",
					style = "TILE",
				},
				isToggleable = false,
				isVisible = true,
			},
			actionPointsBar = {
				anchor1 = { windowName="SELF", point="TOPLEFT", relativeTo="PARENT", relativePoint="TOPLEFT", offsetX="25%", offsetY="65%", },
				anchor2 = { windowName="SELF", point="BOTTOMRIGHT", relativeTo="PARENT", relativePoint="BOTTOMRIGHT", offsetX="0", offsetY="0", },
				growth = "LEFT_RIGHT",
				foreground = {
					isVisible = true,
					color = "ROYAL_BLUE",
					texture = "Otravi",
					style = "FILL",
				},
				background = {
					isVisible = true,
					color = "LIGHT_SLATE_GRAY",
					texture = "WARTintSquare",
					style = "TILE",
				},
				isToggleable = false,
				isVisible = true,
			},
			moraleLevelBar = {
				anchor1 = { windowName="SELF", point="BOTTOMLEFT", relativeTo="PARENT", relativePoint="TOPLEFT", offsetX="25%", offsetY="-10%", },
				anchor2 = { windowName="SELF", point="BOTTOMRIGHT", relativeTo="PARENT", relativePoint="BOTTOMRIGHT", offsetX="0", offsetY="0", },
				growth = "LEFT_RIGHT",
				foreground = {
					isVisible = true,
					color = "DARK_SLATE_GRAY",
					texture = "Otravi",
					style = "FILL",
				},
				background = {
					isVisible = false,
					color = "BLACK",
					texture = "WARTintSquare",
					style = "TILE",
				},
				isToggleable = true,
				isVisible = false,
			},
			spellTrack1 = {
				font = "font_clear_small",
				position = 'TOPLEFT',
				maxWidth = 100,
			},
			spellTrack2 = {
				font = "font_clear_small",
				position = 'LEFT',
				maxWidth = 40,
			},
			labelTopLeft = {
				label = L"[name]",
				font = "font_clear_small_bold",
				paddingX = "25%",
				paddingY = "2",
				color = 'CLASSCOLOR',
				isTargeted = true,
				inAvatarGroup = false,
			},
			labelTop = {
				label = L"[groupleader:post=:][mainassist]",
				font = "font_clear_small",
				paddingX = "43%",
				paddingY = "2",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
			labelTopRight = {
				label = L"",
				font = "font_clear_small",
				paddingX = "0",
				paddingY = "0",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
			labelLeft = {
				label = L"",
				font = "font_clear_small",
				paddingX = "0",
				paddingY = "0",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
			labelCenter = {
				label = L"",
				font = "font_clear_medium_bold",
				paddingX = "0",
				paddingY = "0",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
			labelRight = {
				label = L"[hpperc]%",
				font = "font_clear_small_bold",
				paddingX = "0",
				paddingY = "0",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
			labelBottomLeft = {
				label = L"L[level]",
				font = "font_clear_small_bold",
				paddingX = "1",
				paddingY = "1",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = true,
			},
			labelBottom = {
				label = L"",
				font = "font_clear_small",
				paddingX = "0",
				paddingY = "0",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
			labelBottomRight = {
				label = L"[apperc]%",
				font = "font_clear_small",
				paddingX = "0",
				paddingY = "0",
				color = 'WHITE',
				isTargeted = false,
				inAvatarGroup = false,
			},
		},
		petFrame = {
			height = 20,
			borderWidth = 2,
			borderHeight = 2,
			labelCenter = {
				font = "font_clear_small_bold",
			},
		},
	},

};

HealGridSkin.RegisterSkin( HealGridSkin_LucidMedium, 'MELLI_MEDIUM' );
