HealGridDummyData = {};

-- ############### GROUP ##################################################################################################

HealGridDummyData.groupData =
	{
		
		{
			zoneNum = 102,
			isAssistant = false,
			isRVRFlagged = false,
			isInSameRegion = true,
			level = 29,
			healthPercent = 100,
			moraleLevel = 0,
			Pet = 
			{
				healthPercent = 0,
			},
			online = true,
			actionPointPercent = 100,
			isGroupLeader = true,
			name = L"Jokwaah",
			isMainAssist = true,
			worldObjNum = 0,
			careerName = L"Schamane^m",
			careerLine = 7,
			isMasterLooter = false,
		},
		
		{
			zoneNum = 232,
			isAssistant = false,
			isRVRFlagged = false,
			isInSameRegion = true,
			level = 27,
			healthPercent = 25,
			moraleLevel = 2,
			Pet = 
			{
				healthPercent = 0,
			},
			online = true,
			actionPointPercent = 20,
			isGroupLeader = false,
			name = L"Fahlgren",
			isMainAssist = false,
			worldObjNum = 0,
			careerName = L"Auserkorener^m",
			careerLine = 13,
			isMasterLooter = false,
		},
		
		{
			zoneNum = 232,
			isAssistant = false,
			isRVRFlagged = false,
			isInSameRegion = false,
			level = 30,
			healthPercent = 70,
			moraleLevel = 1,
			Pet = 
			{
				healthPercent = 0,
			},
			online = true,
			actionPointPercent = 100,
			isGroupLeader = false,
			name = L"Thralldirn",
			isMainAssist = false,
			worldObjNum = 0,
			careerName = L"Spalta^m",
			careerLine = 6,
			isMasterLooter = false,
		},
		
		{
			zoneNum = 232,
			isAssistant = false,
			isRVRFlagged = false,
			isInSameRegion = false,
			level = 27,
			healthPercent = 100,
			moraleLevel = 0,
			Pet = 
			{
				healthPercent = 0,
			},
			online = true,
			actionPointPercent = 55,
			isGroupLeader = false,
			name = L"Irias",
			isMainAssist = false,
			worldObjNum = 0,
			careerName = L"Zauberin^f",
			careerLine = 24,
			isMasterLooter = false,
		},
		
		{
			zoneNum = 0,
			isAssistant = false,
			isRVRFlagged = false,
			isInSameRegion = false,
			level = 0,
			healthPercent = 0,
			moraleLevel = 0,
			Pet = 
			{
				healthPercent = 0,
			},
			online = false,
			actionPointPercent = 0,
			isGroupLeader = false,
			name = L"",
			isMainAssist = false,
			worldObjNum = 0,
			careerName = L"",
			careerLine = 0,
			isMasterLooter = false,
		},
	};

-- ############### BATTLEGROUP ############################################################################################

HealGridDummyData.battlegroupData =
	{
		
		{
			players = 
			{
				
				{
					zoneNum = 102,
					isAssistant = true,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 25,
					healthPercent = 77,
					moraleLevel = 0,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 80,
					isGroupLeader = true,
					name = L"Zorgall",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Zelot^m",
					careerLine = 15,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 24,
					healthPercent = 56,
					moraleLevel = 4,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 20,
					isGroupLeader = false,
					name = L"Nokkturnal",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Spalta^m",
					careerLine = 6,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 26,
					healthPercent = 100,
					moraleLevel = 2,
					Pet = 
					{
						healthPercent = 100,
					},
					online = true,
					actionPointPercent = 100,
					isGroupLeader = false,
					name = L"Hokgun",
					isMainAssist = true,
					worldObjNum = 0,
					careerName = L"Squigtreiba^m",
					careerLine = 8,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 23,
					healthPercent = 77,
					moraleLevel = 4,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 40,
					isGroupLeader = false,
					name = L"Lit",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Spalta^m",
					careerLine = 6,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 108,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 21,
					healthPercent = 100,
					moraleLevel = 0,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 100,
					isGroupLeader = false,
					name = L"Mephesti",
					isMainAssist = false,
					worldObjNum = 474,
					careerName = L"schwarze Gardistin^fc",
					careerLine = 21,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 108,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 25,
					healthPercent = 100,
					moraleLevel = 0,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 100,
					isGroupLeader = false,
					name = L"Kallid",
					isMainAssist = false,
					worldObjNum = 78,
					careerName = L"Zelot^m",
					careerLine = 15,
					isMasterLooter = false,
				},
			},
		},
		
		{
			players = 
			{
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 22,
					healthPercent = 100,
					moraleLevel = 3,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 84,
					isGroupLeader = false,
					name = L"Khains",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Zelot^m",
					careerLine = 15,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 23,
					healthPercent = 100,
					moraleLevel = 2,
					Pet = 
					{
						healthPercent = 100,
					},
					online = true,
					actionPointPercent = 2,
					isGroupLeader = false,
					name = L"Chrism",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Magus^m",
					careerLine = 16,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 23,
					healthPercent = 100,
					moraleLevel = 4,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 16,
					isGroupLeader = false,
					name = L"Ulixes",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Auserkorener^m",
					careerLine = 13,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 29,
					healthPercent = 100,
					moraleLevel = 4,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 86,
					isGroupLeader = false,
					name = L"Zia",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Zauberin^f",
					careerLine = 24,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 24,
					healthPercent = 100,
					moraleLevel = 2,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 92,
					isGroupLeader = false,
					name = L"Meccald",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Auserkorener^m",
					careerLine = 13,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 29,
					healthPercent = 100,
					moraleLevel = 2,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 94,
					isGroupLeader = false,
					name = L"Motaa",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Auserkorener^m",
					careerLine = 13,
					isMasterLooter = false,
				},
			},
		},
		
		{
			players = 
			{
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 28,
					healthPercent = 100,
					moraleLevel = 0,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 100,
					isGroupLeader = false,
					name = L"Veshu",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Schamane^m",
					careerLine = 7,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 108,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 26,
					healthPercent = 100,
					moraleLevel = 0,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 100,
					isGroupLeader = false,
					name = L"Zhepeii",
					isMainAssist = false,
					worldObjNum = 472,
					careerName = L"Schamane^m",
					careerLine = 7,
					isMasterLooter = false,
				},
				
				{
					zoneNum = 102,
					isAssistant = false,
					isRVRFlagged = false,
					isInSameRegion = true,
					level = 40,
					healthPercent = 100,
					moraleLevel = 0,
					Pet = 
					{
						healthPercent = 0,
					},
					online = true,
					actionPointPercent = 100,
					isGroupLeader = false,
					name = L"Odehngar",
					isMainAssist = false,
					worldObjNum = 0,
					careerName = L"Auserkorener^m",
					careerLine = 13,
					isMasterLooter = false,
				},
			},
		},
		
		{
			players = 
			{
			},
		},
	};

-- ############### SCENARIOGROUP ##########################################################################################

HealGridDummyData.scenariogroupData =
	{
		
		{
			sgroupslotnum = 2,
			sgroupindex = 1,
			ap = 100,
			careerId = 65,
			name = L"Sterk^M",
			morale = 0,
			career = L"Chaosbarbar^m",
			health = 100,
			isMainAssist = false,
			rank = 28,
		},
		
		{
			sgroupslotnum = 1,
			sgroupindex = 1,
			ap = 100,
			careerId = 65,
			name = L"Kronn^M",
			morale = 0,
			career = L"Chaosbarbar^m",
			health = 100,
			isMainAssist = false,
			rank = 30,
		},
		
		{
			sgroupslotnum = 3,
			sgroupindex = 1,
			ap = 100,
			careerId = 66,
			name = L"Kallid^M",
			morale = 0,
			career = L"Zelot^m",
			health = 100,
			isMainAssist = false,
			rank = 25,
		},
		
		{
			sgroupslotnum = 3,
			sgroupindex = 2,
			ap = 100,
			careerId = 27,
			name = L"Mrekz^M",
			morale = 0,
			career = L"Squigtreiba^m",
			health = 100,
			isMainAssist = false,
			rank = 21,
		},
		
		{
			sgroupslotnum = 4,
			sgroupindex = 2,
			ap = 100,
			careerId = 104,
			name = L"Dekator^M",
			morale = 0,
			career = L"schwarzer Gardist^mc",
			health = 100,
			isMainAssist = false,
			rank = 28,
		},
		
		{
			sgroupslotnum = 4,
			sgroupindex = 1,
			ap = 100,
			careerId = 106,
			name = L"Moonshea^F",
			morale = 0,
			career = L"J�ngerin des Khaine^f",
			health = 100,
			isMainAssist = false,
			rank = 26,
		},
		
		{
			sgroupslotnum = 1,
			sgroupindex = 2,
			ap = 100,
			careerId = 104,
			name = L"Araath^F",
			morale = 10,
			career = L"schwarze Gardistin^fc",
			health = 100,
			isMainAssist = false,
			rank = 21,
		},
		
		{
			sgroupslotnum = 5,
			sgroupindex = 1,
			ap = 100,
			careerId = 26,
			name = L"Macior^M",
			morale = 20,
			career = L"Schamane^m",
			health = 20,
			isMainAssist = false,
			rank = 21,
		},
		
		{
			sgroupslotnum = 2,
			sgroupindex = 2,
			ap = 100,
			careerId = 25,
			name = L"Deathboss^M",
			morale = 50,
			career = L"Spalta^m",
			health = 70,
			isMainAssist = false,
			rank = 24,
		},
		
		{
			sgroupslotnum = 6,
			sgroupindex = 1,
			ap = 100,
			careerId = 106,
			name = L"Amrad^M",
			morale = 100,
			career = L"J�nger des Khaine^m",
			health = 100,
			isMainAssist = false,
			rank = 21,
		},
	};
